<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistroController;
use App\Http\Controllers\ShowCategorias;
use App\Http\Controllers\ArticuloController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('index');
});

Route::get('index', function () {
    return view('index');
});

Route::get('registrarse', function () {
    return view('registrarse');
});
*/

Route::view('/', 'auth.login')->name('auth.login');
//Route::view('/index', 'index')->name('index');
Route::get('/index', [App\Http\Controllers\ArticuloController::class, 'landing'])->name('index');
Route::view('/registrarse', 'registrarse')->name('registrarse');
Route::get('/agregar', [App\Http\Controllers\ShowCategorias::class, 'index'])->name('agregar');
Route::get('/indexAdmin', [App\Http\Controllers\ArticuloController::class, 'index'])->name('indexAdmin');
Route::view('/borradores', 'borradores')->name('borradores');
Route::view('/buscador', 'buscador')->name('buscador');
Route::view('/carrito', 'carrito')->name('carrito');
Route::view('/detalle', 'detalle')->name('detalle');
//Route::view('/indexAdmin', 'indexAdmin')->name('indexAdmin');
//Route::view('/modificar', 'modificar')->name('modificar');
Route::get('/modificar/{id}', [App\Http\Controllers\ArticuloController::class, 'modify'])->name('modificar');
Route::get('/detalles/{id}', [App\Http\Controllers\ArticuloController::class, 'details'])->name('detalles');
Route::view('/solicitudes', 'solicitudes')->name('solicitudes');
Route::view('/ventas', 'ventas')->name('ventas');

Route::post('detalles/{id}', [ArticuloController::class, 'comentar'])->name('detalles');

Route::get('/like/{id}', [App\Http\Controllers\ArticuloController::class, 'likes'])->name('like');

Route::post('registrarse', [RegistroController::class,'store']);

Route::post('buscador', [ArticuloController::class,'buscar']);

Route::post('addprod', [ArticuloController::class,'store']);
Route::post('editprod', [ArticuloController::class,'update']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
