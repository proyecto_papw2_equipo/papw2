<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('descripcion');
            $table->integer('unidades')->unsigned();
            $table->string('img1');
            $table->string('img2');
            $table->string('img3');
            $table->string('video');
            $table->enum('estado', array('borrador', 'publicado'));
            $table->enum('existe', array('si', 'no'))->default('si');
            $table->foreignId('categoria_id')->constrained('categorias');
            $table->foreignId('usuario_id')->constrained('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
