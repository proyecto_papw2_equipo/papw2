<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'descripcion',
        'unidades',
        'img1',
        'img2',
        'img3',
        'video',
        'categoria_id',
        'estado',
        'usuario_id',
    ];
}
