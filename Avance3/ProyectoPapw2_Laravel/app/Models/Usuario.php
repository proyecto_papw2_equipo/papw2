<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    //Los campos que pueden llenarse
    protected $fillable = [
        'nombre',
        'apellido',
        'correo',
        'nickname',
        'contrasena',
        'telefono',
        'calle',
        'colonia',
        'estado',
        'avatar',
        'portada'
    ];

}
