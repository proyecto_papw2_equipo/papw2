<?php

namespace App\Http\Controllers;
use App\Models\Categoria;

use Illuminate\Http\Request;

class ShowCategorias extends Controller
{
    public function index(){
        $categoria = Categoria::get();
        return view('agregar', compact('categoria'));
    }
}
