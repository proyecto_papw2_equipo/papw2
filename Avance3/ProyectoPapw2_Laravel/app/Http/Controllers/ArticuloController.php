<?php

namespace App\Http\Controllers;
use App\Models\Articulo;
use App\Models\Categoria;
use App\Models\User;
use App\Models\Comentario;
use App\Models\Like;

use Illuminate\Http\Request;

class ArticuloController extends Controller
{
    public function store(Request $request){
        $articulo  =  new Articulo();
        $articulo->nombre =  $request->input('nombre');
        $articulo->descripcion =  $request->input('descripcion');
        $articulo->unidades =  $request->input('unidades');
        $articulo->categoria_id =  $request->input('categCombo');
        $articulo->estado =  $request->input('estadoCombo');
        $articulo->usuario_id =  auth()->user()->id;

        if($request->hasFile('img1')){
            $file = $request->file('img1');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . 'img1.' . $extension;
            $file->move('images/imagenes/', $filename);
            $articulo->img1 = $filename;
        } else {
            $articulo->img1 = '';
        }

        if($request->hasFile('img2')){
            $file = $request->file('img2');
            $extension = $file->getClientOriginalExtension();
            $filename = time() .  'img2.' . $extension;
            $file->move('images/imagenes/', $filename);
            $articulo->img2 = $filename;
        } else {
            $articulo->img2 = '';
        }

        if($request->hasFile('img3')){
            $file = $request->file('img3');
            $extension = $file->getClientOriginalExtension();
            $filename = time() .  'img3.' . $extension;
            $file->move('images/imagenes/', $filename);
            $articulo->img3 = $filename;
        } else {
            $articulo->img3 = '';
        }

        if($request->hasFile('video')){
            $file = $request->file('video');
            $extension = $file->getClientOriginalExtension();
            $filename = time() .  'vid.' . $extension;
            $file->move('images/videos/', $filename);
            $articulo->video = $filename;
        } else {
            $articulo->video = '';
        }

        $articulo->save();

        return redirect()->route('index');
    }

    public function index(){
        $articulo = Articulo::where('usuario_id', auth()->user()->id)->get();
        return view('indexAdmin', compact('articulo'));
    }

    public function modify($id){
        $articulo = Articulo::findOrFail($id);
        $categoria = Categoria::get();
        return view('modificar', ['articulo' => $articulo], compact('categoria'));
    }

    public function update(Request $request){
        $articulo  =  Articulo::find($request->input('id'));
        $articulo->nombre =  $request->input('nombre');
        $articulo->descripcion =  $request->input('descripcion');
        $articulo->unidades =  $request->input('unidades');
        $articulo->categoria_id =  $request->input('categCombo');
        $articulo->estado =  $request->input('estadoCombo');

        if($request->hasFile('img1')){
            $file = $request->file('img1');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . 'img1.' . $extension;
            $file->move('images/imagenes/', $filename);
            $articulo->img1 = $filename;
        }

        if($request->hasFile('img2')){
            $file = $request->file('img2');
            $extension = $file->getClientOriginalExtension();
            $filename = time() .  'img2.' . $extension;
            $file->move('images/imagenes/', $filename);
            $articulo->img2 = $filename;
        }

        if($request->hasFile('img3')){
            $file = $request->file('img3');
            $extension = $file->getClientOriginalExtension();
            $filename = time() .  'img3.' . $extension;
            $file->move('images/imagenes/', $filename);
            $articulo->img3 = $filename;
        }

        if($request->hasFile('video')){
            $file = $request->file('video');
            $extension = $file->getClientOriginalExtension();
            $filename = time() .  'vid.' . $extension;
            $file->move('images/videos/', $filename);
            $articulo->video = $filename;
        }

        $articulo->save();

        return redirect()->route('index');
    }

    public function landing(){
        $articuloCarousel = Articulo::where('estado', 'publicado')->where('existe', 'si')->orderByDesc('created_at')->limit(3)->get();
        $articuloCard = Articulo::where('estado', 'publicado')->where('existe', 'si')->orderBy('created_at', 'asc')->limit(3)->get();
        return view('index', compact('articuloCarousel', 'articuloCard'));
    }

    public function details($id){
        $articulo = Articulo::findOrFail($id);
        $categoria = Categoria::findOrFail($articulo->categoria_id);
        $usuario = User::findOrFail($articulo->usuario_id);
        $comentarios = Comentario::select('comentarios.texto', 'comentarios.created_at', 'users.name')->join('users', 'users.id', '=', 'comentarios.usuario_id')->where('articulo_id', $id)->orderByDesc('created_at')->get();
        $likes = Like::select('id')->where('articulo_id', $id)->get()->count();
        $validar = Like::where('usuario_id', auth()->user()->id)->where('articulo_id', $id)->get()->count();
        return view('detalles', ['articulo' => $articulo, 'usuario' => $usuario, 'categoria' => $categoria, 'comentarios' => $comentarios, 'likes' => $likes, 'validar' => $validar]);
    }

    public function comentar(Request $request, $id){
        $comentario  =  new Comentario();
        $comentario->texto =  $request->input('comentario');
        $comentario->articulo_id =  $id;
        $comentario->usuario_id =  auth()->user()->id;

        $comentario->save();

        $articulo = Articulo::findOrFail($comentario->articulo_id);
        $categoria = Categoria::findOrFail($articulo->categoria_id);
        $usuario = User::findOrFail($articulo->usuario_id);
        $comentarios = Comentario::select('comentarios.texto', 'comentarios.created_at', 'users.name')->join('users', 'users.id', '=', 'comentarios.usuario_id')->where('articulo_id', $id)->orderByDesc('created_at')->get();
        $likes = Like::select('id')->where('articulo_id', $id)->get()->count();
        $validar = Like::where('usuario_id', auth()->user()->id)->where('articulo_id', $id)->get()->count();
        return view('detalles', ['articulo' => $articulo, 'usuario' => $usuario, 'categoria' => $categoria, 'comentarios' => $comentarios, 'likes' => $likes, 'validar' => $validar]);
    }

    public function likes($id){
        $like  =  new Like();
        $like->articulo_id =  $id;
        $like->usuario_id =  auth()->user()->id;

        $like->save();

        $articulo = Articulo::findOrFail($id);
        $categoria = Categoria::findOrFail($articulo->categoria_id);
        $usuario = User::findOrFail($articulo->usuario_id);
        $comentarios = Comentario::select('comentarios.texto', 'comentarios.created_at', 'users.name')->join('users', 'users.id', '=', 'comentarios.usuario_id')->where('articulo_id', $id)->orderByDesc('created_at')->get();
        $likes = Like::select('id')->where('articulo_id', $id)->get()->count();
        $validar = Like::where('usuario_id', auth()->user()->id)->where('articulo_id', $id)->get()->count();
        return view('detalles', ['articulo' => $articulo, 'usuario' => $usuario, 'categoria' => $categoria, 'comentarios' => $comentarios, 'likes' => $likes, 'validar' => $validar]);
    }

    public function buscar(Request $request){
        $articulos = Articulo::where('nombre', 'like', '%' . $request->input('buscado') . '%')->where('existe', 'si')->orderByDesc('created_at')->get();
        return view('buscador', compact('articulos'));
    }

}
