<?php

namespace App\Http\Controllers;
use App\Models\Usuario;

use Illuminate\Http\Request;

class RegistroController extends Controller
{
    //
    public function store(Request $request){
        $usuario  =  new Usuario();
        $usuario->nombre =  $request->input('nombre');
        $usuario->apellido =  $request->input('apellido');
        $usuario->correo =  $request->input('correo');
        $usuario->nickname =  $request->input('nickname');
        $usuario->contrasena =  bcrypt($request->input('contrasena'));
        $usuario->telefono =  $request->input('telefono');
        $usuario->calle =  $request->input('calle');
        $usuario->colonia =  $request->input('colonia');
        $usuario->estado =  $request->input('estado');

        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $extension = $file->getClientOriginalExtension();
            $filename = time() .  '.' . $extension;
            $file->move('images/avatar/', $filename);
            $usuario->avatar = $filename;
        } else {
            $usuario->avatar = '';
        }

        if($request->hasFile('portada')){
            $file = $request->file('portada');
            $extension = $file->getClientOriginalExtension();
            $filename = time() .  '.' . $extension;
            $file->move('images/portada/', $filename);
            $usuario->portada = $filename;
        } else {
            $usuario->portada = '';
        }

        $usuario->save();

        return redirect()->route('index');
    }

}
