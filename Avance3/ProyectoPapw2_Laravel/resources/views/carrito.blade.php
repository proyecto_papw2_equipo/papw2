<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
	<script src="{{asset('js/app.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
	<!--Navbar-->
	@include('navbar')
	<!--Resultados del carrito-->
	<div style="margin-left: 5%; margin-top: 2%">
        <h2>Carrito</h2>
    </div>

    <div class="card mb-3" style="max-width: 70%; margin-left: 5%; margin-top: 1%" display: inline-block>
        <div class="row no-gutters">
            <div class="col-md-10">
                <h4>Total en el carrito: $00.00</h4>
            </div>
            <div class="col-md-2">
                <a href="#" class="btn btn-success float-center">Comprar</a>
            </div>
        </div>
    </div>

    <div class="card mb-3" style="max-width: 70%; margin-left: 5%; margin-top: 1%" display: inline-block>
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="{{asset('images/default/slide.jpg')}}" class="card-img" alt="300">
            </div>
            <div class="col-md-6">
                <div class="card-body">
                    <h5 class="card-title" align="left">Nombre del juego</h5>
                    <p class="card-text">Aqui se va a escribir una breve sinopsis del juego</p>
                    <p class="card-text"><small class="text-muted">Aqui se va a escribir la fecha en la que se publicó en la página</small></p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card-body">
                    <p class="card-title" align="right" style="margin-right: 10%;">$00.00</p>
                    <a href="{{ url('carrito') }}" class="btn btn-outline-success">Eliminar</a>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
