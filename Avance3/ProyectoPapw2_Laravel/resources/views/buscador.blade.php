<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
	<script src="{{asset('js/app.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
	<!--Navbar-->
	@include('navbar')
	<!--Tabla-->
	<div class="row">
		<aside class="col-sm-2">
			<div class="card" style="padding-left: 5%;">
  				<div class="card-header">
    				Categor&iacuteas
  				</div>
  				<div class="card-body">
  					<input type="checkbox" value="alimentos"> Alimentos y Bebidas<br>
  					<input type="checkbox" value="animales"> Animales y Mascotas<br>
  					<input type="checkbox" value="belleza"> Belleza<br>
					<input type="checkbox" value="deportes"> Deportes<br>
					<input type="checkbox" value="electrodomesticos"> Electrodomesticos<br>
  					<input type="checkbox" value="electronica"> Electronica<br>
  					<input type="checkbox" value="hogar"> Hogar<br>
  					<input type="checkbox" value="musica"> Musica<br>
  					<input type="checkbox" value="juegos"> Juegos<br>
  					<input type="checkbox" value="libros"> Libros<br>
  					<input type="checkbox" value="refacciones"> Refacciones<br>
  					<input type="checkbox" value="ropa"> Ropa<br>
  					<input type="checkbox" value="software"> Software<br>
  					<input type="checkbox" value="videojuegos"> Videojuegos<br>
  					<input type="checkbox" value="otros"> Otros<br>
  				</div>
			</div>
		</aside>
		<div class="col-sm-10">
			<h2 style="margin-top: 2%; margin-bottom: 2%;">Resultados de la b&uacutesqueda</h2>
            <!--Resultado de la busqueda-->
            @forelse ($articulos as $item)
            <div class="card mb-3" style="max-width: 90%; padding-left: 5%;">
                <a href="{{ route('detalles', $item) }}">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{asset('images/imagenes/' . $item->img1)}}" class="card-img" alt="...">
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <h5 class="card-title" align="left">{{ $item->nombre }}</h5>
                                <p class="card-text"><small class="text-muted">{{ $item->created_at }}</small></p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @empty
            <h3>No hay productos con ese nombre</h3>
            @endforelse


		</div>
	</div>
</body>
</html>
