<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
	<script src="{{asset('js/app.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
	<!--Navbar-->
	@include('navbar')

	<h3 class="ml-5 mr-5 mt-3 mb-3">Modificar producto</h3>

	<form class="ml-5 mr-5 mt-3 mb-3" method="POST" action="{{ url('editprod') }}" enctype="multipart/form-data">
        @csrf
        <input id="prodId" name="id" type="hidden" value="{{ $articulo->id }}">
        <div class="form-group">
    		<label for="nombreProd">Nombre del producto</label>
    		<input type="text" name="nombre" class="form-control" id="nombreProd" placeholder="Nombre" required value="{{ $articulo->nombre }}">
		</div>
		<div class="form-group">
    		<label for="descProd">Descripci&oacuten</label>
    		<textarea class="form-control" name="descripcion" id="descProd" rows="3" required placeholder="Detalles del producto">{{ $articulo->descripcion }}</textarea>
		</div>
		<div class="form-group">
    		<label for="uniProd">Unidades</label>
    		<input type="text" class="form-control" name="unidades" id="uniProd" required placeholder="0" value="{{ $articulo->unidades }}">
		</div>
		<div class="form-row">
			<div class="col">
				<div class="form-group" align="center">
					<label for="File1">Imagen 1</label>
					<input type="file" class="form-control-file" name="img1" id="File1">
					<img src="{{asset('images/imagenes/' . $articulo->img1)}}" alt="Imagen 1" width="300" class="mt-3 mb-3">
				</div>
			</div>
			<div class="col">
				<div class="form-group" align="center">
					<label for="File2">Imagen 2</label>
					<input type="file" class="form-control-file" name="img2" id="File2">
					<img src="{{asset('images/imagenes/' . $articulo->img2)}}" alt="Imagen 2" width="300" class="mt-3 mb-3">
				</div>
			</div>
			<div class="col">
				<div class="form-group" align="center">
					<label for="File3">Imagen 3</label>
					<input type="file" class="form-control-file" name="img3" id="File3">
					<img src="{{asset('images/imagenes/' . $articulo->img3)}}" alt="Imagen 3" width="300" class="mt-3 mb-3">
				</div>
			</div>
		</div>
		<div class="form-group">
    		<label for="vidProd">Video</label>
    		<input type="file" class="form-control-file" id="vidProd">
    		<video controls src="{{asset('images/videos/' . $articulo->video)}}" width="500" class="mt-3"></video>
		</div>
		<div class="form-group">
			<label for="categProd">Categoria</label>
			<select class="form-control" name="categCombo" id="categProd" required>
				@forelse ($categoria as $item)
                <option value="{{ $item->id }}"
                    @if ($item->id == $articulo->categoria_id)
                        selected
                    @endif
                    > {{ $item->nombre }}</option>
                @empty
                <option value="0"> No hay categorias</option>
                @endforelse
			</select>
		</div>
		<div class="form-group">
			<label for="estadoProd">Guardar como borrador o publicar el producto:</label>
			<select class="form-control" name="estadoCombo" id="estadoProd" required>
                <option value="borrador"
                @if ($articulo->estado == 'borrador')
                    selected
                @endif
                > Guardar como borrador</option>
                <option value="publicado"
                @if ($articulo->estado == 'publicado')
                    selected
                @endif
                > Publicar</option>
			</select>
		</div>
		<div class="form-group mt-2 mb-4" style="float: right;">
			<a href="{{ route('indexAdmin') }}" class="btn btn-outline-success">Cancelar</a>
			<button type="submit" class="btn btn-success">Guardar</button>
		</div>
	</form>

</body>
</html>
