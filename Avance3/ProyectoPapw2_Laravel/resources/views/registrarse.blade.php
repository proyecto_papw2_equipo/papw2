<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
    <!--script src="JQuery 3.3.1/jquery-3.3.1.min.js" type="text/javascript"></script-->
    <script src="{{asset('js/app.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
	<!--Navbar-->
	@include('navbar')


	<h3 class="ml-5 mr-5 mt-3 mb-3">Registro de usuario</h3>

    <form class="ml-5 mr-5 mt-3 mb-3" method="POST" action="{{ url('registrarse') }}" enctype="multipart/form-data">
        @csrf
		<div class="form-row">
			<div class="col">
				<div class="form-group">
		    		<label for="nombre">Nombre</label>
		    		<input name="nombre" type="text" class="form-control" id="nombre" placeholder="Nombre" required>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
		    		<label for="apellido">Apellido</label>
		    		<input name="apellido" type="text" class="form-control" id="apellido" placeholder="Apellido" required>
				</div>
			</div>
		</div>
		<div class="form-row">
			<div class="col">
				<div class="form-group">
		    		<label for="correo">Correo electr&oacutenico</label>
		    		<input name="correo" type="text" class="form-control" id="correo" placeholder="example@domain.com" required>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
		    		<label for="username">Username</label>
		    		<input name="nickname" type="text" class="form-control" id="username" placeholder="nickname123" required>
				</div>
			</div>
		</div>
		<div class="form-row">
			<div class="col">
				<div class="form-group">
		    		<label for="contra">Contraseña</label>
		    		<input name="contrasena" type="password" class="form-control" id="contra" placeholder="**********" required>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
		    		<label for="tel">Tel&eacutefono</label>
		    		<input name="telefono" type="text" class="form-control" id="tel" placeholder="8123456789" required>
				</div>
			</div>
		</div>
		<div class="form-row">
			<div class="col">
				<div class="form-group">
		    		<label for="calle">Calle</label>
		    		<input name="calle" type="text" class="form-control" id="calle" placeholder="Calle del domicilio" required>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
		    		<label for="colonia">Colonia</label>
		    		<input name="colonia" type="text" class="form-control" id="Colonia" placeholder="Colonia del domicilio" required>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
		    		<label for="estado">Estado</label>
		    		<input name="estado" type="text" class="form-control" id="estado" placeholder="Estado del domicilio" required>
				</div>
			</div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="form-group">
                    <label for="selAvatar">Avatar</label>
                    <input name="avatar" type="file" class="form-control-file" id="selAvatar" accept="image/*" required>
                    <img src="{{asset('images/default/slide.jpg')}}" alt="Imagen" height="150" class="mt-3 mb-3" id="imgAvatar">
                    <script>
                        const $seleccionArchivos = document.querySelector("#selAvatar"),
                        $imagenPrevisualizacion = document.querySelector("#imgAvatar");
                        $seleccionArchivos.addEventListener("change", () => {
                            const archivos = $seleccionArchivos.files;
                            if (!archivos || !archivos.length) {
                                $imagenPrevisualizacion.src = "";
                                return;
                            }
                            const primerArchivo = archivos[0];
                            const objectURL = URL.createObjectURL(primerArchivo);
                            $imagenPrevisualizacion.src = objectURL;
                        });
                    </script>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="selPortada">Portada</label>
                    <input name="portada" type="file" class="form-control-file" id="selPortada" accept="image/*" required>
                    <img src="{{asset('images/default/slide.jpg')}}" alt="Imagen" height="150" class="mt-3 mb-3" id="imgPortada">
                    <script>
                        const $seleccionArchivos2 = document.querySelector("#selPortada"),
                        $imagenPrevisualizacion2 = document.querySelector("#imgPortada");
                        $seleccionArchivos2.addEventListener("change", () => {
                            const archivos2 = $seleccionArchivos2.files;
                            if (!archivos2 || !archivos2.length) {
                                $imagenPrevisualizacion2.src = "";
                                return;
                            }
                            const primerArchivo2 = archivos2[0];
                            const objectURL2 = URL.createObjectURL(primerArchivo2);
                            $imagenPrevisualizacion2.src = objectURL2;
                        });
                    </script>
                </div>
            </div>
        </div>

		<div class="form-group mt-2 mb-4" style="float: right;">
			<button type="button" class="btn btn-outline-success mr-3" onclick="location.href='{{ url('index') }}';">Cancelar</button>
			<button type="submit" class="btn btn-success">Registrarse</button>
		</div>
    </form>

</body>
</html>



<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>




<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-jet-button class="ml-4">
                    {{ __('Login') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
