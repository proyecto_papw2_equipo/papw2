<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Vendetodo</title>
    <script src="{{asset('js/app.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/detalles.css')}}" type="text/css">

    <style>
        #crearCuenta{
            margin-right: 1%;
            margin-left: 1%;
        }
        #iniciarSesion{
            margin-right: 1%;
            margin-left: 1%;
        }


    </style>
    <script>
        $(document).ready(function(){
            //document.getElementById("usuario").disabled = true;
        });
    </script>
</head>
<body>
    <!--Navbar-->
    @include('navbar')

    <div class="row justify-content-center">
        <div id="carouselNotiRec" class="carousel slide col-8" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{asset('images/imagenes/' . $articulo->img1)}}" class="d-block w-100" alt="First slide" />
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/imagenes/' . $articulo->img2)}}" class="d-block w-100" alt="Second slide" />
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/imagenes/' . $articulo->img3)}}" class="d-block w-100" alt="Third slide" />
                </div>
                <div class="carousel-item">
                    <video class="d-block w-100" controls>
                        <source id="sourceVid2" type="video/mp4"
                            src="{{asset('images/videos/' . $articulo->video)}}" >
                    </video>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselNotiRec" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselNotiRec" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="feho">
        <p class="fechapub">En venta por {{ $usuario->name }} en la categoria {{ $categoria->nombre }}</p>
    </div>
    <div class="datosNoticia">
        <h2 class="titulo">{{ $articulo->nombre }}</h2>
        <p>{{ $articulo->descripcion }}</p>
    </div>

    <div class="social">
        <p class="like">¿Te gusta este producto?&nbsp;
            @if ($validar == 0)
            <a href="{{ url('like', $articulo->id) }}">👍</a>
            @endif
            --- {{ $likes }} likes</p>
        <h3>Comentarios</h3>

        <form action="{{ url('detalles', $articulo->id) }}" method="POST">
            @csrf
            <div class="form-group">
                <input id="prodId" name="articulo" type="hidden" value="{{ $articulo->id }}">
                <label for="comentarioPersona">Deja tu comentario</label>
                <textarea class="form-control" id="comentarioPersona" name="comentario" rows="3" required></textarea>
                <p class="text-right vermas" type="button"><input class="btn btn-dark float-right" type="submit" value="Enviar comentario" name="btnGuardar" /></p><br><br>
            </div>
        </form>
    </div>

    @forelse ($comentarios as $comentario)
    <div class="card coment">
        <div class="card-body">
            <div>
                <h5 class="card-title">{{ $comentario->name }}</h5>
            </div>
            <div class="textocoment">
                <p class="card-text">{{ $comentario->texto }}</p>
            </div>
        </div>
    </div>
    @empty
    <div class="card coment">
        <div class="card-body">
            <div>
                <h5 class="card-title">No hay comentarios</h5>
            </div>
        </div>
    </div>
    @endforelse

</body>
</html>
