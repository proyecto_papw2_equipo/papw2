<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
    <!--script src="JQuery 3.3.1/jquery-3.3.1.min.js" type="text/javascript"></script-->
    <script src="{{asset('js/app.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
    <!--Navbar-->
    @include('navbar')

	<!--carrusel-->
	<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="6000">
        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            {{
                $varCont = 0
            }}
            @forelse ($articuloCarousel as $item)
            <div class="carousel-item
            @if ($varCont == 0)
            active
            {{ $varCont = 1 }}
            @endif
            ">
                <a href="{{ route('detalles', $item) }}">
                    <picture class="row justify-content-center">
                        <img srcset="{{asset('images/imagenes/' . $item->img1)}}" alt="responsive image" class="d-block img-fluid">
                    </picture>
                    <div class="carousel-caption">
                        <div>
                            <h2>{{ $item->nombre }}</h2>
                        </div>
                    </div>
                </a>
            </div>
            @empty
                <h1>No hay artículos disponibles</h1>
            @endforelse
        </div>
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->
<!-- /.container -->
<!--Cards-->
	<h3 style="margin-left: 2.5%"><br>Los mas antiguo<br></h3>
    @forelse ($articuloCard as $card)
    <div class="card" style="width: 30%; float: left; margin-left: 2.5%;" onclick="location.href='{{ route('detalles', $card) }}';">
        <img src="{{asset('images/imagenes/' . $card->img1)}}" class="card-img-top" alt="...">
        <div class="card-body">
          <p class="card-text">{{ $card->nombre }}</p>
        </div>
  </div>
    @empty
    <h3 style="margin-left: 2.5%"><br>No hay articulos publicados<br></h3>
    @endforelse



</body>
</html>
