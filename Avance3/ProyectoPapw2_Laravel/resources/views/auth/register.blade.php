<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
    <!--script src="JQuery 3.3.1/jquery-3.3.1.min.js" type="text/javascript"></script-->
    <script src="{{asset('js/app.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
	<!--Navbar-->

	<h3 class="ml-5 mr-5 mt-3 mb-3">Registro de usuario</h3>

    <form class="ml-5 mr-5 mt-3 mb-3" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
        @csrf
		<div class="form-row">
			<div class="col">
				<div class="form-group">
		    		<label for="nombre">Nombre</label>
		    		<input name="name" type="text" class="form-control" id="nombre" placeholder="Nombre" required>
				</div>
			</div>
		</div>
		<div class="form-row">
			<div class="col">
				<div class="form-group">
		    		<label for="correo">Correo electr&oacutenico</label>
		    		<input name="email" type="text" class="form-control" id="correo" placeholder="example@domain.com" required>
				</div>
			</div>
		</div>
		<div class="form-row">
			<div class="col">
				<div class="form-group">
		    		<label for="contra">Contraseña</label>
		    		<input name="password" type="password" class="form-control" id="contra" placeholder="**********" required>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
		    		<label for="contra2">Confirmar contraseña</label>
		    		<input name="password_confirmation" type="password" class="form-control" id="contra2" placeholder="**********" required>
				</div>
			</div>
		</div>
        <div class="form-group mt-2 mb-4" style="float: right;">
            <a href="{{ route('login') }}" class="btn btn-outline-success">Ya estas registrado?</a>
			<button type="submit" class="btn btn-success">Registrarse</button>
		</div>
    </form>

</body>
</html>
