<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="{{ url('index') }}">Vendetodo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
    </button>
     <div class="collapse navbar-collapse" id="navbarSupportedContent">
       <ul class="navbar-nav mr-auto">
             <li class="nav-item">
               <a class="nav-link" href="{{ url('buscador') }}">Tienda</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="{{ url('carrito') }}">Carrito</a>
             </li>
       </ul>
       <ul class="navbar-nav mr-auto">
           <form class="form-inline my-2 my-lg-0" action="{{ url('buscador') }}" method="POST">
                @csrf
                 <input class="form-control mr-sm-2" type="search" name="buscado" placeholder="Buscar..." aria-label="Search">
                 <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
           </form>
       </ul>
       <!--
       <button id="crearCuenta" class="btn btn-outline-success my-2 my-sm-0" type="button">Crear Cuenta</button>
       <button id="iniciarSesion" class="btn btn-success my-2 my-sm-0" type="button" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Aqui los datos para iniciar sesión">Iniciar Sesión</button>-->
       <ul class="navbar-nav">
           @guest
            <li id="crearCuenta" class="nav-item">
               <a class="btn btn-outline-success my-2 my-sm-0" href="{{ url('registrarse') }}">Registrarse</a>
            </li>
            <li id="iniciarSesion" class="nav-item dropleft">
                <button class="dropdown-toggle btn btn-success my-2 my-sm-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Iniciar Sesi&oacuten</button>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <form class="px-4 py-3" method="POST" action="">
                        @csrf
                        <label for="username">Usuario</label>
                        <input class="drop" type="text" id="username" placeholder="Username">
                        <label for="password">Contrase&ntildea</label>
                        <input class="drop" type="password" id="password" placeholder="*****"><br>
                        <button class="btn btn-success my-2 my-sm-0">Entrar</button>
                    </form>
                </div>
            </li>
           @endguest
           @auth
            <li class="nav-item dropdown dropleft">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{auth()->user()->name}}</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Perfil</a>
                    <a class="dropdown-item" href="#">Historial</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ url('indexAdmin') }}">Mis Productos</a>
                    <div class="dropdown-divider"></div>
                    <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-jet-dropdown-link href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                            this.closest('form').submit();">
                                {{ __('Logout') }}
                            </x-jet-dropdown-link>
                        </form>
                </div>
            </li>
            @endauth
       </ul>
     </div>
</nav>
