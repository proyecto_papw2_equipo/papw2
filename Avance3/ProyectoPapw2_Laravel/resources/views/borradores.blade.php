<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
	<script src="{{asset('js/app.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
	<!--Navbar-->
	@include('navbar')

	<form class="ml-5 mr-5 mt-4" action="#" onsubmit="javascript:alert('Jala');">
		<div class="form-row">
			<div class="form-group col-md-2">
				<select class="form-control" name="categCombo" onChange="javascript:alert(this.value);">
					<option value="todos"> Todos</option>
					<option value="alimentos"> Alimentos y Bebidas</option>
		  			<option value="animales"> Animales y Mascotas</option>
		  			<option value="belleza"> Belleza</option>
					<option value="deportes"> Deportes</option>
					<option value="electrodomesticos"> Electrodomesticos</option>
		  			<option value="electronica"> Electronica</option>
		  			<option value="hogar"> Hogar</option>
		  			<option value="musica"> Musica</option>
		  			<option value="juegos"> Juegos</option>
		  			<option value="libros"> Libros</option>
		  			<option value="refacciones"> Refacciones</option>
		  			<option value="ropa"> Ropa</option>
		  			<option value="software"> Software</option>
		  			<option value="videojuegos"> Videojuegos</option>
		  			<option value="otros"> Otros</option>
				</select>
			</div>
			<div class="form-group col-md-10">
				<input type="text" class="form-control" id="nameProd" placeholder="Nombre del producto">
			</div>
		</div>
	</form>

	<div class="card mb-3 mt-3 ml-5 mr-5">
		<div class="row no-gutters">
			<div class="col-md-4">
			    <img src="{{asset('images/default/slide.jpg')}}" class="card-img" alt="...">
			</div>
			<div class="col-md-6">
                <div class="card-body">
                    <h5 class="card-title" align="left">Nombre del Producto</h5>
                    <p class="card-text">Descripcion del ptoducto</p>
                    <p class="card-text"><small class="text-muted">Fecha de creacion</small></p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card-body">
                    <p class="card-title" align="right" style="margin-right: 10%;">$00.00</p>
                    <a href="modificar.html" class="btn btn-outline-success sel mb-3">Modificar<span class="jId" style="display: none;">FuncionGetId</span><span class="jName" style="display: none;">FuncionGetName</span></a><br>
                    <a href="#" class="btn btn-outline-success sel mb-3">Eliminar<span class="jId" style="display: none;">FuncionGetId</span><span class="jName" style="display: none;">FuncionGetName</span></a><br>
                    <a href="#" class="btn btn-outline-success sel mb-3">Publicar<span class="jId" style="display: none;">FuncionGetId</span><span class="jName" style="display: none;">FuncionGetName</span></a>
                </div>
            </div>
		</div>
	</div>

</body>
</html>
