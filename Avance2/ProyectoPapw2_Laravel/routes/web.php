<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistroController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('index');
});

Route::get('index', function () {
    return view('index');
});

Route::get('registrarse', function () {
    return view('registrarse');
});
*/

Route::view('/', 'index')->name('index');
Route::view('/index', 'index')->name('index');
Route::view('/registrarse', 'registrarse')->name('registrarse');
Route::view('/agregar', 'agregar')->name('agregar');
Route::view('/borradores', 'borradores')->name('borradores');
Route::view('/buscador', 'buscador')->name('buscador');
Route::view('/carrito', 'carrito')->name('carrito');
Route::view('/detalle', 'detalle')->name('detalle');
Route::view('/indexAdmin', 'indexAdmin')->name('indexAdmin');
Route::view('/modificar', 'modificar')->name('modificar');
Route::view('/solicitudes', 'solicitudes')->name('solicitudes');
Route::view('/ventas', 'ventas')->name('ventas');

Route::post('registrarse', [RegistroController::class,'store']);
