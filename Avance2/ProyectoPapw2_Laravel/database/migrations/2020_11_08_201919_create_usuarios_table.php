<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('correo',70)->unique();
            $table->string('nickname',50)->unique();
            $table->string('contrasena');
            $table->string('telefono');
            $table->string('calle');
            $table->string('colonia');
            $table->string('estado');
            $table->string('avatar');
            $table->string('portada');
            $table->enum('rol', array('user', 'admin'))->default('user');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
