<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
	<script src="{{asset('js/app.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
	<!--Navbar-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url('index') }}">Vendetodo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
        </button>
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
           <ul class="navbar-nav mr-auto">
                 <li class="nav-item">
                   <a class="nav-link" href="{{ url('buscador') }}">Tienda</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="{{ url('carrito') }}">Carrito</a>
                 </li>
                 <li class="nav-item dropdown">
                   <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NombreUsuario</a>
                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                         <a class="dropdown-item" href="#">Perfil</a>
                         <a class="dropdown-item" href="#">Historial</a>
                         <div class="dropdown-divider"></div>
                         <a class="dropdown-item" href="{{ url('indexAdmin') }}">Mis Productos</a>
                         <div class="dropdown-divider"></div>
                         <a class="dropdown-item" href="{{ url('index') }}">Cerrar Sesión</a>
                   </div>
                 </li>
           </ul>
           <ul class="navbar-nav mr-auto">
               <form class="form-inline my-2 my-lg-0" action="{{ url('buscador') }}">
                     <input class="form-control mr-sm-2" type="search" placeholder="Buscar..." aria-label="Search">
                     <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
               </form>
           </ul>
           <!--
           <button id="crearCuenta" class="btn btn-outline-success my-2 my-sm-0" type="button">Crear Cuenta</button>
           <button id="iniciarSesion" class="btn btn-success my-2 my-sm-0" type="button" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Aqui los datos para iniciar sesión">Iniciar Sesión</button>-->
           <ul class="navbar-nav">
               <li id="crearCuenta" class="nav-item">
                   <a class="btn btn-outline-success my-2 my-sm-0" href="{{ url('registrarse') }}">Registrarse</a>
                 </li>
               <li id="iniciarSesion" class="nav-item dropleft">
                   <button class="dropdown-toggle btn btn-success my-2 my-sm-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Iniciar Sesi&oacuten</button>
                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                       <form class="px-4 py-3">
                             Usuario
                             <input class="drop" type="text" placeholder="Username">
                             Contrase&ntildea
                             <input class="drop" type="password" placeholder="*****">
                             <button class="btn btn-success my-2 my-sm-0">Entrar</button>
                         </form>
                   </div>
                 </li>
           </ul>
         </div>
   </nav>

	<h3 class="ml-5 mr-5 mt-3 mb-3">Modificar producto</h3>

	<form class="ml-5 mr-5 mt-3 mb-3">
		<div class="form-group">
    		<label for="nombreProd">Nombre del producto</label>
    		<input type="text" class="form-control" id="nombreProd" placeholder="Nombre">
		</div>
		<div class="form-group">
    		<label for="descProd">Descripci&oacuten</label>
    		<textarea class="form-control" id="descProd" rows="3" placeholder="Detalles del producto"></textarea>
		</div>
		<div class="form-group">
    		<label for="uniProd">Unidades</label>
    		<input type="text" class="form-control" id="uniProd" placeholder="0">
		</div>
		<div class="form-row">
			<div class="col">
				<div class="form-group" align="center">
					<label for="File1">Imagen 1</label>
					<input type="file" class="form-control-file" id="File1">
					<img src="{{asset('images/default/slide.jpg')}}" alt="Imagen 1" width="300" class="mt-3 mb-3">
				</div>
			</div>
			<div class="col">
				<div class="form-group" align="center">
					<label for="File2">Imagen 2</label>
					<input type="file" class="form-control-file" id="File2">
					<img src="{{asset('images/default/slide.jpg')}}" alt="Imagen 2" width="300" class="mt-3 mb-3">
				</div>
			</div>
			<div class="col">
				<div class="form-group" align="center">
					<label for="File3">Imagen 3</label>
					<input type="file" class="form-control-file" id="File3">
					<img src="{{asset('images/default/slide.jpg')}}" alt="Imagen 3" width="300" class="mt-3 mb-3">
				</div>
			</div>
		</div>
		<div class="form-group">
    		<label for="vidProd">Video</label>
    		<input type="file" class="form-control-file" id="vidProd">
    		<video src="" poster="{{asset('images/default/slide.jpg')}}" width="500" class="mt-3"></video>
		</div>
		<div class="form-group">
			<label for="categProd">Categoria</label>
			<select class="form-control" name="categCombo" id="categProd">
				<option value="alimentos"> Alimentos y Bebidas</option>
		  		<option value="animales"> Animales y Mascotas</option>
		  		<option value="belleza"> Belleza</option>
				<option value="deportes"> Deportes</option>
				<option value="electrodomesticos"> Electrodomesticos</option>
		  		<option value="electronica"> Electronica</option>
		  		<option value="hogar"> Hogar</option>
		  		<option value="musica"> Musica</option>
		  		<option value="juegos"> Juegos</option>
		  		<option value="libros"> Libros</option>
		  		<option value="refacciones"> Refacciones</option>
		  		<option value="ropa"> Ropa</option>
		  		<option value="software"> Software</option>
		  		<option value="videojuegos"> Videojuegos</option>
		  		<option value="otros"> Otros</option>
			</select>
		</div>
		<div class="form-group">
			<label for="estadoProd">Guardar como borrador o publicar el producto:</label>
			<select class="form-control" name="estadoCombo" id="estadoProd">
				<option value="borrador"> Guardar como borrador</option>
				<option value="publicar"> Publicar</option>
			</select>
		</div>
		<div class="form-group mt-2 mb-4" style="float: right;">
			<button type="button" class="btn btn-outline-success mr-3">Cancelar</button>
			<button type="submit" class="btn btn-success">Agregar producto</button>
		</div>
	</form>

</body>
</html>
