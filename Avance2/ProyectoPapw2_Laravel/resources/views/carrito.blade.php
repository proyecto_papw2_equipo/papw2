<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
	<script src="{{asset('js/app.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
	<!--Navbar-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url('index') }}">Vendetodo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
        </button>
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
           <ul class="navbar-nav mr-auto">
                 <li class="nav-item">
                   <a class="nav-link" href="{{ url('buscador') }}">Tienda</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="{{ url('carrito') }}">Carrito</a>
                 </li>
                 <li class="nav-item dropdown">
                   <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NombreUsuario</a>
                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                         <a class="dropdown-item" href="#">Perfil</a>
                         <a class="dropdown-item" href="#">Historial</a>
                         <div class="dropdown-divider"></div>
                         <a class="dropdown-item" href="{{ url('indexAdmin') }}">Mis Productos</a>
                         <div class="dropdown-divider"></div>
                         <a class="dropdown-item" href="{{ url('index') }}">Cerrar Sesión</a>
                   </div>
                 </li>
           </ul>
           <ul class="navbar-nav mr-auto">
               <form class="form-inline my-2 my-lg-0" action="{{ url('buscador') }}">
                     <input class="form-control mr-sm-2" type="search" placeholder="Buscar..." aria-label="Search">
                     <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
               </form>
           </ul>
           <!--
           <button id="crearCuenta" class="btn btn-outline-success my-2 my-sm-0" type="button">Crear Cuenta</button>
           <button id="iniciarSesion" class="btn btn-success my-2 my-sm-0" type="button" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Aqui los datos para iniciar sesión">Iniciar Sesión</button>-->
           <ul class="navbar-nav">
               <li id="crearCuenta" class="nav-item">
                   <a class="btn btn-outline-success my-2 my-sm-0" href="{{ url('registrarse') }}">Registrarse</a>
                 </li>
               <li id="iniciarSesion" class="nav-item dropleft">
                   <button class="dropdown-toggle btn btn-success my-2 my-sm-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Iniciar Sesi&oacuten</button>
                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                       <form class="px-4 py-3">
                             Usuario
                             <input class="drop" type="text" placeholder="Username">
                             Contrase&ntildea
                             <input class="drop" type="password" placeholder="*****">
                             <button class="btn btn-success my-2 my-sm-0">Entrar</button>
                         </form>
                   </div>
                 </li>
           </ul>
         </div>
   </nav>
	<!--Resultados del carrito-->
	<div style="margin-left: 5%; margin-top: 2%">
        <h2>Carrito</h2>
    </div>

    <div class="card mb-3" style="max-width: 70%; margin-left: 5%; margin-top: 1%" display: inline-block>
        <div class="row no-gutters">
            <div class="col-md-10">
                <h4>Total en el carrito: $00.00</h4>
            </div>
            <div class="col-md-2">
                <a href="#" class="btn btn-success float-center">Comprar</a>
            </div>
        </div>
    </div>

    <div class="card mb-3" style="max-width: 70%; margin-left: 5%; margin-top: 1%" display: inline-block>
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="{{asset('images/default/slide.jpg')}}" class="card-img" alt="300">
            </div>
            <div class="col-md-6">
                <div class="card-body">
                    <h5 class="card-title" align="left">Nombre del juego</h5>
                    <p class="card-text">Aqui se va a escribir una breve sinopsis del juego</p>
                    <p class="card-text"><small class="text-muted">Aqui se va a escribir la fecha en la que se publicó en la página</small></p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card-body">
                    <p class="card-title" align="right" style="margin-right: 10%;">$00.00</p>
                    <a href="{{ url('carrito') }}" class="btn btn-outline-success">Eliminar</a>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
