<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Vendetodo</title>
    <!--script src="JQuery 3.3.1/jquery-3.3.1.min.js" type="text/javascript"></script-->
    <script src="{{asset('js/app.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
    	#crearCuenta{
    		margin-right: 1%;
    		margin-left: 1%;
    	}
    	#iniciarSesion{
    		margin-right: 1%;
    		margin-left: 1%;
    	}


    </style>
    <script>
    	$(document).ready(function(){
    		//document.getElementById("usuario").disabled = true;
    	});
	</script>
</head>
<body>
	<!--Navbar-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url('index') }}">Vendetodo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
        </button>
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
           <ul class="navbar-nav mr-auto">
                 <li class="nav-item">
                   <a class="nav-link" href="{{ url('buscador') }}">Tienda</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="{{ url('carrito') }}">Carrito</a>
                 </li>
                 <li class="nav-item dropdown">
                   <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NombreUsuario</a>
                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                         <a class="dropdown-item" href="#">Perfil</a>
                         <a class="dropdown-item" href="#">Historial</a>
                         <div class="dropdown-divider"></div>
                         <a class="dropdown-item" href="{{ url('indexAdmin') }}">Mis Productos</a>
                         <div class="dropdown-divider"></div>
                         <a class="dropdown-item" href="{{ url('index') }}">Cerrar Sesión</a>
                   </div>
                 </li>
           </ul>
           <ul class="navbar-nav mr-auto">
               <form class="form-inline my-2 my-lg-0" action="{{ url('buscador') }}">
                     <input class="form-control mr-sm-2" type="search" placeholder="Buscar..." aria-label="Search">
                     <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
               </form>
           </ul>
           <!--
           <button id="crearCuenta" class="btn btn-outline-success my-2 my-sm-0" type="button">Crear Cuenta</button>
           <button id="iniciarSesion" class="btn btn-success my-2 my-sm-0" type="button" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Aqui los datos para iniciar sesión">Iniciar Sesión</button>-->
           <ul class="navbar-nav">
               <li id="crearCuenta" class="nav-item">
                   <a class="btn btn-outline-success my-2 my-sm-0" href="{{ url('registrarse') }}">Registrarse</a>
                 </li>
               <li id="iniciarSesion" class="nav-item dropleft">
                   <button class="dropdown-toggle btn btn-success my-2 my-sm-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Iniciar Sesi&oacuten</button>
                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                       <form class="px-4 py-3">
                             Usuario
                             <input class="drop" type="text" placeholder="Username">
                             Contrase&ntildea
                             <input class="drop" type="password" placeholder="*****">
                             <button class="btn btn-success my-2 my-sm-0">Entrar</button>
                         </form>
                   </div>
                 </li>
           </ul>
         </div>
   </nav>

	<!--carrusel-->
	<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="6000">
        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <a href="https://bootstrapcreative.com/">
                    <!--
                    If you need more browser support use https://scottjehl.github.io/picturefill/
                    If a picture looks blurry on a retina device you can add a high resolution like this
                    <source srcset="img/blog-post-1000x600-2.jpg, blog-post-1000x600-2@2x.jpg 2x" media="(min-width: 768px)">

                    What image sizes should you use? This can help - https://codepen.io/JacobLett/pen/NjramL
                     -->
                     <picture>
                      <source srcset="https://dummyimage.com/2000x500/007aeb/4196e5" media="(min-width: 1400px)">
                      <source srcset="https://dummyimage.com/1400x500/#007aeb/4196e5" media="(min-width: 769px)">
                       <source srcset="https://dummyimage.com/800x500/007aeb/4196e5" media="(min-width: 577px)">
                      <img srcset="https://dummyimage.com/600x500/007aeb/4196e5" alt="responsive image" class="d-block img-fluid">
                    </picture>

                    <div class="carousel-caption">
                        <div>
                            <h2>Hola mundo</h2>

                        </div>
                    </div>
                </a>
            </div>
            <!-- /.carousel-item -->
            <div class="carousel-item">
                <a href="https://bootstrapcreative.com/">
                     <picture>
                      <source srcset="https://dummyimage.com/2000x500/007aeb/4196e5" media="(min-width: 1400px)">
                      <source srcset="https://dummyimage.com/1400x500/007aeb/4196e5" media="(min-width: 769px)">
                       <source srcset="https://dummyimage.com/800x500/007aeb/4196e5" media="(min-width: 577px)">
                      <img srcset="https://dummyimage.com/600x500/007aeb/4196e5" alt="responsive image" class="d-block img-fluid">
                    </picture>

                    <div class="carousel-caption justify-content-center align-items-center">
                        <div>
                            <h2>Item importante</h2>
                        </div>
                    </div>
                </a>
            </div>
            <!-- /.carousel-item -->
            <div class="carousel-item">
                <a href="https://bootstrapcreative.com/">
                     <picture>
                      <source srcset="https://dummyimage.com/2000x500/007aeb/4196e5" media="(min-width: 1400px)">
                      <source srcset="https://dummyimage.com/1400x500/007aeb/4196e5" media="(min-width: 769px)">
                       <source srcset="https://dummyimage.com/800x500/007aeb/4196e5" media="(min-width: 577px)">
                      <img srcset="https://dummyimage.com/600x500/007aeb/4196e5" alt="responsive image" class="d-block img-fluid">
                    </picture>

                    <div class="carousel-caption justify-content-center align-items-center">
                        <div>
                        	<h2>Publicidad</h2>
                        </div>
                    </div>
                </a>
            </div>
            <!-- /.carousel-item -->
        </div>
        <!-- /.carousel-inner -->
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->
<!-- /.container -->

	<!--Cards-->
	<h3 style="margin-left: 2.5%"><br>Los mas populares<br></h3>
	<div class="card" style="width: 30%; float: left; margin-left: 2.5%;" onclick="location.href='{{ url('detalle') }}';">
  		<img src="{{asset('images/default/slide.jpg')}}" class="card-img-top" alt="...">
  		<div class="card-body">
    		<p class="card-text">Titulo Objeto</p>
    		<p class="card-text">Categoría</p>
  		</div>
	</div>
	<div class="card" style="width: 30%; float: left; margin-left: 2.5%; margin-right: 2.5%;" onclick="location.href='{{ url('detalle') }}';">
  		<img src="{{asset('images/default/slide.jpg')}}" class="card-img-top" alt="...">
  		<div class="card-body">
    		<p class="card-text">Titulo Objeto</p>
    		<p class="card-text">Categoría</p>
		</div>
	</div>
	<div class="card" style="width: 30%; float: left; margin-right: 2.5%; margin-bottom: 2.5%;" onclick="location.href='{{ url('detalle') }}';">
		<img src="{{asset('images/default/slide.jpg')}}" class="card-img-top" alt="...">
		<div class="card-body">
	  		<p class="card-text">Titulo Objeto</p>
    		<p class="card-text">Categoría</p>
		</div>
	</div>

	<!--Cards-->
	<h3 style="margin-left: 2.5%; padding-top: 2.5%;">Los mejor calificados</h3>
	<div class="card" style="width: 30%; float: left; margin-left: 2.5%;" onclick="location.href='{{ url('detalle') }}';">
  		<img src="{{asset('images/default/slide.jpg')}}" class="card-img-top" alt="...">
  		<div class="card-body">
    		<p class="card-text">Titulo Objeto</p>
    		<p class="card-text">Categoría</p>
  		</div>
	</div>
	<div class="card" style="width: 30%; float: left; margin-left: 2.5%; margin-right: 2.5%;" onclick="location.href='{{ url('detalle') }}';">
  		<img src="{{asset('images/default/slide.jpg')}}" class="card-img-top" alt="...">
  		<div class="card-body">
    		<p class="card-text">Titulo Objeto</p>
    		<p class="card-text">Categoría</p>
		</div>
	</div>
	<div class="card" style="width: 30%; float: left; margin-right: 2.5%; margin-bottom: 2.5%;" onclick="location.href='{{ url('detalle') }}';">
		<img src="{{asset('images/default/slide.jpg')}}" class="card-img-top" alt="...">
		<div class="card-body">
	  		<p class="card-text">Titulo Objeto</p>
    		<p class="card-text">Categoría</p>
        </div>

</body>
</html>
